jQuery(function ($) {





    'use strict';

    /************setting page*************/
        $(document).ready(function() {

      		$('.trgmultislect, .SelectpengaturanTani,.SelectpengaturanHutan,.SelectpengaturanPerikanan,.SelectpengaturanLainnya').attr('disabled',true);

          $('.trgslect').on('change',function() {
              if($('.trgslect').is(':checked')){
                $('.trgmultislect').removeAttr('disabled');
              }else{
                $('.trgmultislect').val('');
                $('.trgmultislect').attr('disabled', true);
                $('.ygdtrg').hide();
              }
          });

      		$('#pengaturanTaniCheckbox').on('change', function () {


      					if ($('#pengaturanTaniCheckbox').is(':checked')) {
      						$('.SelectpengaturanTani').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanTani').val('');
      							$('.SelectpengaturanTani').attr('disabled', true);
      							$(".multiSelectPertanian").hide();

      					}

      		});

      		$('#pengaturanHutanCheckbox').on('change', function () {


      					if ($('#pengaturanHutanCheckbox').is(':checked')) {
      						$('.SelectpengaturanHutan').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanHutan').val("");
      							$('.SelectpengaturanHutan').attr('disabled', true);
      							$(".multiSelectPerhutanan").hide();
      					}

      		});

      		$('#pengaturanIkanCheckbox').on('change', function () {


      					if ($('#pengaturanIkanCheckbox').is(':checked')) {
      						$('.SelectpengaturanPerikanan').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanPerikanan').val("");
      							$('.SelectpengaturanPerikanan').attr('disabled', true);
      							$(".multiSelectPerrikanan").hide();
      					}

      		});

      		$('#pengaturanLainyaCheckbox').on('change', function () {


      					if ($('#pengaturanLainyaCheckbox').is(':checked')) {
      						$('.SelectpengaturanLainnya').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanLainnya').val("");
      							$('.SelectpengaturanLainnya').attr('disabled', true);
      							$(".multiSelectLainnya").hide();
      					}

      		});

        });


      	$(".ygdtrg, .multiSelectPertanian,.multiSelectPerhutanan,.multiSelectPerrikanan,.multiSelectLainnya").hide();

          $('.trgmultislect').change(function() {
            if($('.trgmultislect').val() == ""){
      				$('.ygdtrg').hide();
      			}
      			else {
      				$('.ygdtrg').show();
      			}
          });

          $('.SelectpengaturanTani').change(function() {
      			if($('.SelectpengaturanTani').val() == ""){
      				$(".multiSelectPertanian").hide();
      			}
      			else {
      				$(".multiSelectPertanian").show();
      			}

      		});
      		$(".SelectpengaturanHutan").change(function() {
      			if($(".SelectpengaturanHutan").val() == ""){
      				$(".multiSelectPerhutanan").hide();
      			}
      			else {
      				$(".multiSelectPerhutanan").show();
      			}

      		});
      		$(".SelectpengaturanPerikanan").change(function() {
      			if($(".SelectpengaturanPerikanan").val() == ""){
      				$(".multiSelectPerrikanan").hide();
      			}
      			else {
      				$(".multiSelectPerrikanan").show();
      			}

      		});
      		$(".SelectpengaturanLainnya").change(function() {
      			if($(".SelectpengaturanLainnya").val() == ""){
      				$(".multiSelectLainnya").hide();
      			}
      			else {
      				$(".multiSelectLainnya").show();
      			}

      		});

      		/*****************Newsletter****************/

      		$(document).ready(function() {

      		$('.SelectpengaturanNewsletterTani,.SelectpengaturanNewsletterHutan,.SelectpengaturanNewsletterPerikanan,.SelectpengaturanNewsletterLainnya').attr('disabled',true);

      		$('#pengaturanNewsletterTaniCheckbox').on('change', function () {


      					if ($('#pengaturanNewsletterTaniCheckbox').is(':checked')) {
      						$('.SelectpengaturanNewsletterTani').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanNewsletterTani').val('');
      							$('.SelectpengaturanNewsletterTani').attr('disabled', true);
      							$(".multiSelectPertanianNewsletter").hide();

      					}

      		});

      		$('#pengaturanNewsletterHutanCheckbox').on('change', function () {


      					if ($('#pengaturanNewsletterHutanCheckbox').is(':checked')) {
      						$('.SelectpengaturanNewsletterHutan').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanNewsletterHutan').val("");
      							$('.SelectpengaturanNewsletterHutan').attr('disabled', true);
      							$(".multiSelectPerhutananNewsletter").hide();
      					}

      		});

      		$('#pengaturanNewsletterIkanCheckbox').on('change', function () {


      					if ($('#pengaturanNewsletterIkanCheckbox').is(':checked')) {
      						$('.SelectpengaturanNewsletterPerikanan').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanNewsletterPerikanan').val("");
      							$('.SelectpengaturanNewsletterPerikanan').attr('disabled', true);
      							$(".multiSelectPerrikananNewsletter").hide();
      					}

      		});

      		$('#pengaturanNewsletterLainyaCheckbox').on('change', function () {


      					if ($('#pengaturanNewsletterLainyaCheckbox').is(':checked')) {
      						$('.SelectpengaturanNewsletterLainnya').removeAttr('disabled');

      					} else {
      							$('.SelectpengaturanNewsletterLainnya').val("");
      							$('.SelectpengaturanNewsletterLainnya').attr('disabled', true);
      							$(".multiSelectLainnyaNewsletter").hide();
      					}

      		});

        });


      	$(".multiSelectPertanianNewsletter,.multiSelectPerhutananNewsletter,.multiSelectPerrikananNewsletter,.multiSelectLainnyaNewsletter").hide();
      		$('.SelectpengaturanNewsletterTani').change(function() {
      			if($('.SelectpengaturanNewsletterTani').val() == ""){
      				$(".multiSelectPertanianNewsletter").hide();
      			}
      			else {
      				$(".multiSelectPertanianNewsletter").show();
      			}

      		});
      		$(".SelectpengaturanNewsletterHutan").change(function() {
      			if($(".SelectpengaturanNewsletterHutan").val() == ""){
      				$(".multiSelectPerhutananNewsletter").hide();
      			}
      			else {
      				$(".multiSelectPerhutananNewsletter").show();
      			}

      		});
      		$(".SelectpengaturanNewsletterPerikanan").change(function() {
      			if($(".SelectpengaturanNewsletterPerikanan").val() == ""){
      				$(".multiSelectPerrikananNewsletter").hide();
      			}
      			else {
      				$(".multiSelectPerrikananNewsletter").show();
      			}

      		});
      		$(".SelectpengaturanNewsletterLainnya").change(function() {
      			if($(".SelectpengaturanNewsletterLainnya").val() == ""){
      				$(".multiSelectLainnyaNewsletter").hide();
      			}
      			else {
      				$(".multiSelectLainnyaNewsletter").show();
      			}

      		});
    /**********setting page end***********/

    /*Edit Profile script */
          $(document).ready(function() {

              handleNamaOrangPt();
              handlePilihSektor();

            });

        /*lokasi*/
        function handleNamaOrangPt() {
          $(".trigPt, .trigOr").change(function(){
              $(".orInputOr, .ptInputPt").val("").attr("readonly",true);
              if($(".trigPt").is(":checked")){
                  $(".ptInputPt").removeAttr("readonly");
                  $(".ptInputPt").focus();
              }
              else if($(".trigOr").is(":checked")){
                  $(".orInputOr").removeAttr("readonly");
                  $(".orInputOr").focus();
              }
          });
        }

        /*sektor*/
        $('.selectprofileSettingTani,.selectprofileSettingHutan,.selectprofileSettingIkan,.selectprofileSettingLainya').attr('disabled',true);
        $(".multiSelectProfilePertanian,.multiSelectProfilePerhutanan,.multiSelectProfilePerikanan,.multiSelectProfileLainnya").hide();
        function handlePilihSektor() {

          //tani
          $('.ProfileSektorTaniCheckbox').on('change', function () {


      					if ($('.ProfileSektorTaniCheckbox').is(':checked')) {
      						$('.selectprofileSettingTani').removeAttr('disabled');

      					} else {
      							$('.selectprofileSettingTani').val('nol');
      							$('.selectprofileSettingTani').attr('disabled', true);
      							$('.multiSelectProfilePertanian').hide();

      					}

      		});

          $('.selectprofileSettingTani').change(function() {
      			if($('.selectprofileSettingTani').val() == "nol"){
      				$('.multiSelectProfilePertanian').hide();
      			}
      			else {
      				$('.multiSelectProfilePertanian').show();
      			}

      		});

          //Hutan
          $('.ProfileSektorHutanCheckbox').on('change', function () {


      					if ($('.ProfileSektorHutanCheckbox').is(':checked')) {
      						$('.selectprofileSettingHutan').removeAttr('disabled');

      					} else {
      							$('.selectprofileSettingHutan').val('nol');
      							$('.selectprofileSettingHutan').attr('disabled', true);
      							$('.multiSelectProfilePerhutanan').hide();

      					}

      		});

          $('.selectprofileSettingHutan').change(function() {
      			if($('.selectprofileSettingHutan').val() == "nol"){
      				$('.multiSelectProfilePerhutanan').hide();
      			}
      			else {
      				$('.multiSelectProfilePerhutanan').show();
      			}

      		});

          //Ikan
          $('.ProfileSektorIkanCheckbox').on('change', function () {


      					if ($('.ProfileSektorIkanCheckbox').is(':checked')) {
      						$('.selectprofileSettingIkan').removeAttr('disabled');

      					} else {
      							$('.selectprofileSettingIkan').val('nol');
      							$('.selectprofileSettingIkan').attr('disabled', true);
      							$('.multiSelectProfilePerikanan').hide();

      					}

      		});

          $('.selectprofileSettingIkan').change(function() {
      			if($('.selectprofileSettingIkan').val() == "nol"){
      				$('.multiSelectProfilePerikanan').hide();
      			}
      			else {
      				$('.multiSelectProfilePerikanan').show();
      			}

      		});

          //lainya
          $('.ProfileSektorLainyaCheckbox').on('change', function () {


      					if ($('.ProfileSektorLainyaCheckbox').is(':checked')) {
      						$('.selectprofileSettingLainya').removeAttr('disabled');

      					} else {
      							$('.selectprofileSettingLainya').val('nol');
      							$('.selectprofileSettingLainya').attr('disabled', true);
      							$('.multiSelectProfileLainnya').hide();

      					}

      		});

          $('.selectprofileSettingLainya').change(function() {
      			if($('.selectprofileSettingLainya').val() == "nol"){
      				$('.multiSelectProfileLainnya').hide();
      			}
      			else {
      				$('.multiSelectProfileLainnya').show();
      			}

      		});

        }

          $('.select2').select2({
              maximumSelectionLength: 5,
              tags: true,
              tokenSeparators: [',']
          });


          /*enable pilih kategori*/






            function toggleSector(checkboxID, toggleID) {
              var checkbox = document.getElementById(checkboxID);
              var toggle = document.getElementById(toggleID);

              if (checkbox.checked = true) {
                toggle.removeAttr('disabled');
              } else {
                  toggle.setAttribute('disabled','true');
              }
            }

          /*js sektor kategori ke select2
          	var data = [];
          	$(document).on('change','#SelectSektorTani', function () {
          		 if ($(this).val()!=null && $(this).val()!='') {
          				 if ($.inArray($(this).val(), data) != -1) {
          				 } else {
          						 data.push($(this).val());
          						 $('#pertanianSektor').tagsinput('destroy');

          						 $('#pertanianSektor').val(data);
          						 $('#pertanianSektor').tagsinput('refresh')


          				 }

          				 $('#pertanianSektor').on('itemRemoved', function(event) {
          				   data=[]
          				 });


          		 }
          	}); */

            /*submit penawaran script */

            /*nama item*/
            $('.selectNamaItemPenawaran').select2({
                maximumSelectionLength: 5,
                tags: true,
                tokenSeparators: [','],
                placeholder: "Masukan nama item anda"
            });
            /*submit permintaan script*/

            /*nama item*/
            $('.selectNamaItemPermintaan').select2({
                maximumSelectionLength: 5,
                tags: true,
                tokenSeparators: [','],
                placeholder: "Masukan nama item anda"
            });








    $(document).ready(function() {
    function close_mobile_menu_list_section() {
        $('.mobile_menu_list .mobile_menu_list-section-title').removeClass('active');
        $('.mobile_menu_list .mobile_menu_list-section-content').slideUp(300).removeClass('open');
    }

    $('.mobile_menu_list-section-title').click(function(e) {

            var currentAttrValue = $(this).attr('href');

            if($(e.target).is('.active')) {
                close_mobile_menu_list_section();
            }else {
                close_mobile_menu_list_section();


                $(this).addClass('active');

                $('.mobile_menu_list ' + currentAttrValue).slideDown(300).addClass('open');
            }
            e.preventDefault();
        });
    });

    $(document).ready(function() {
    function close_mobile_submenu_list_section() {
        $('.mobile_submenu_list .mobile_submenu_list-section-title').removeClass('active');
        $('.mobile_submenu_list .mobile_submenu_list-section-content').slideUp(300).removeClass('open');
    }

    $('.mobile_submenu_list-section-title').click(function(e) {

            var currentAttrValue = $(this).attr('href');

            if($(e.target).is('.active')) {
                close_mobile_submenu_list_section();
            }else {
                close_mobile_submenu_list_section();


                $(this).addClass('active');

                $('.mobile_submenu_list ' + currentAttrValue).slideDown(300).addClass('open');
            }
            e.preventDefault();
        });
    });

    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        animationLoop: false,
        slideshow: false,
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });

    $(document).ready(function(){
          $('.productCarousel').slick({
          dots: false,
          infinite: true,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }

          ]
        });
    });

    $(document).ready(function(){
          $('.productTerkait').slick({
          dots: false,
          infinite: true,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }

          ]
        });
    });



    //tab
    $(document).ready(function(){


    });
    var tabSet1=document.querySelector('.tabset1');

  $('.tabset1').click(function(){
    var tab_id1 = $(this).attr('data-tab1');

    $('.tabset1').removeClass('current1');
    $('.tab-content1').removeClass('current1');

    $(this).addClass('current1');
    $("#"+tab_id1).addClass('current1');

  });
    /*Tab setting*/
      //tab
    $(document).ready(function(){
      var tabSet=document.querySelector('.tabset');

    $('.tabset').click(function(){
      var tab_id = $(this).attr('data-tab');

      $('.tabset').removeClass('current');
      $('.tab-content').removeClass('current');

      $(this).addClass('current');
      $("#"+tab_id).addClass('current');

    });

    });

    /*Tab setting*/
      //tab
    $(document).ready(function(){
      var tabSet2=document.querySelector('.tabset2');

    $('.tabset2').click(function(){
      var tab_id2 = $(this).attr('data-tab2');

      $('.tabset2').removeClass('current2');
      $('.tab-content2').removeClass('current2');

      $(this).addClass('current2');
      $("#"+tab_id2).addClass('current2');

    });

    });





    $(document).find(".ratting").each(function (i,e) {
                   var rating = $(this).data('rating');
                   $(this).rateYo({
                       starWidth : "12px",
                       rating: rating,
                       readOnly: true,
                       ratedFill: "#ffd600 "
                   });
               });
    /***********product-detail Image Gallery************/
          var initPhotoSwipeFromDOM = function(gallerySelector) {

                // parse slide data (url, title, size ...) from DOM elements
                // (children of gallerySelector)
                var parseThumbnailElements = function(el) {
                    var thumbElements = el.childNodes,
                        numNodes = thumbElements.length,
                        items = [],
                        figureEl,
                        linkEl,
                        size,
                        item;

                    for(var i = 0; i < numNodes; i++) {

                        figureEl = thumbElements[i]; // <figure> element

                        // include only element nodes
                        if(figureEl.nodeType !== 1) {
                            continue;
                        }

                        linkEl = figureEl.children[0]; // <a> element

                        size = linkEl.getAttribute('data-size').split('x');

                        // create slide object
                        item = {
                            src: linkEl.getAttribute('href'),
                            w: parseInt(size[0], 10),
                            h: parseInt(size[1], 10)
                        };



                        if(figureEl.children.length > 1) {
                            // <figcaption> content
                            item.title = figureEl.children[1].innerHTML;
                        }

                        if(linkEl.children.length > 0) {
                            // <img> thumbnail element, retrieving thumbnail url
                            item.msrc = linkEl.children[0].getAttribute('src');
                        }

                        item.el = figureEl; // save link to element for getThumbBoundsFn
                        items.push(item);
                    }

                    return items;
                };

                // find nearest parent element
                var closest = function closest(el, fn) {
                    return el && ( fn(el) ? el : closest(el.parentNode, fn) );
                };

                // triggers when user clicks on thumbnail
                var onThumbnailsClick = function(e) {
                    e = e || window.event;
                    e.preventDefault ? e.preventDefault() : e.returnValue = false;

                    var eTarget = e.target || e.srcElement;

                    // find root element of slide
                    var clickedListItem = closest(eTarget, function(el) {
                        return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                    });

                    if(!clickedListItem) {
                        return;
                    }

                    // find index of clicked item by looping through all child nodes
                    // alternatively, you may define index via data- attribute
                    var clickedGallery = clickedListItem.parentNode,
                        childNodes = clickedListItem.parentNode.childNodes,
                        numChildNodes = childNodes.length,
                        nodeIndex = 0,
                        index;

                    for (var i = 0; i < numChildNodes; i++) {
                        if(childNodes[i].nodeType !== 1) {
                            continue;
                        }

                        if(childNodes[i] === clickedListItem) {
                            index = nodeIndex;
                            break;
                        }
                        nodeIndex++;
                    }



                    if(index >= 0) {
                        // open PhotoSwipe if valid index found
                        openPhotoSwipe( index, clickedGallery );
                    }
                    return false;
                };

                // parse picture index and gallery index from URL (#&pid=1&gid=2)
                var photoswipeParseHash = function() {
                    var hash = window.location.hash.substring(1),
                    params = {};

                    if(hash.length < 5) {
                        return params;
                    }

                    var vars = hash.split('&');
                    for (var i = 0; i < vars.length; i++) {
                        if(!vars[i]) {
                            continue;
                        }
                        var pair = vars[i].split('=');
                        if(pair.length < 2) {
                            continue;
                        }
                        params[pair[0]] = pair[1];
                    }

                    if(params.gid) {
                        params.gid = parseInt(params.gid, 10);
                    }

                    return params;
                };

                var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                    var pswpElement = document.querySelectorAll('.pswp')[0],
                        gallery,
                        options,
                        items;

                    items = parseThumbnailElements(galleryElement);

                    // define options (if needed)
                    options = {

                        // define gallery index (for URL)
                        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                        getThumbBoundsFn: function(index) {
                            // See Options -> getThumbBoundsFn section of documentation for more info
                            var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                                pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                rect = thumbnail.getBoundingClientRect();

                            return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                        }

                    };

                    // PhotoSwipe opened from URL
                    if(fromURL) {
                        if(options.galleryPIDs) {
                            // parse real index when custom PIDs are used
                            // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                            for(var j = 0; j < items.length; j++) {
                                if(items[j].pid == index) {
                                    options.index = j;
                                    break;
                                }
                            }
                        } else {
                            // in URL indexes start from 1
                            options.index = parseInt(index, 10) - 1;
                        }
                    } else {
                        options.index = parseInt(index, 10);
                    }

                    // exit if index not found
                    if( isNaN(options.index) ) {
                        return;
                    }

                    if(disableAnimation) {
                        options.showAnimationDuration = 0;
                    }

                    // Pass data to PhotoSwipe and initialize it
                    gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                    gallery.init();
                };

                // loop through all gallery elements and bind events
                var galleryElements = document.querySelectorAll( gallerySelector );

                for(var i = 0, l = galleryElements.length; i < l; i++) {
                    galleryElements[i].setAttribute('data-pswp-uid', i+1);
                    galleryElements[i].onclick = onThumbnailsClick;
                }

                // Parse URL and open gallery if it contains #&pid=3&gid=1
                var hashData = photoswipeParseHash();
                if(hashData.pid && hashData.gid) {
                    openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
                }
            };

            // execute above function
            initPhotoSwipeFromDOM('.my-gallery');
    /***********product-detail Image Gallery End************/

    $(document).ready(function() {

            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:6,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                enableTouch:true,
                enableDrag:true,
                freeMove:true,
                swipeThreshold: 40,
                responsive : [],
                addClass: '',
                mode: "slide",
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////
                  onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }
            });
		});


      $('#thumbcarousel').carousel(0);
      var $thumbItems = $('#thumbcarousel .item');
      $('#carousel').carousel({
          pause: true,
          interval: false
      });
      $('#carousel').on('slide.bs.carousel', function (event) {
         var $slide = $(event.relatedTarget);
         var thumbIndex = $slide.data('thumb');
         var curThumbIndex = $thumbItems.index($('#thumbcarousel .item.active').get(0));
          if (curThumbIndex>thumbIndex) {
              $('#thumbcarousel').one('slid.bs.carousel', function (event) {
                  $('#thumbcarousel').carousel(thumbIndex);
              });
              if (curThumbIndex === ($thumbItems.length-1)) {
                  $('#thumbcarousel').carousel('next');
              } else {
                  $('#thumbcarousel').carousel(numThumbItems-1);
              }
          } else {
              $('#thumbcarousel').carousel(thumbIndex);
          }
      });




    // -------------------------------------------------------------
    // Preloader
    // -------------------------------------------------------------
    (function () {
        $('#status').fadeOut();
        $('#preloader').delay(200).fadeOut('slow');
    }());



    // ------------------------------------------------------------------
    // sticky menu
    // ------------------------------------------------------------------
	$(window).scroll(function() {
	    if ($(".navbar").offset().top > 330) {
	        $(".navbar-fixed-top").addClass("sticky-nav");
	    } else {
	        $(".navbar-fixed-top").removeClass("sticky-nav");
	    }
	});



    // -------------------------------------------------------------
    // mobile menu
    // -------------------------------------------------------------
    (function () {
        $('button.navbar-toggle').ucOffCanvasMenu({
        documentWrapper: '#main-wrapper',
        contentWrapper : '.content-wrapper',
        position       : 'uc-offcanvas-left',    // class name
        // opener         : 'st-menu-open',            // class name
        effect         : 'slide-along',             // class name
        closeButton    : '#uc-mobile-menu-close-btn',
        menuWrapper    : '.uc-mobile-menu',                 // class name below-pusher
        documentPusher : '.uc-mobile-menu-pusher'
        });
    }());




    // -------------------------------------------------------------
    // tooltip
    // -------------------------------------------------------------

    (function () {

        $('[data-toggle="tooltip"]').tooltip()

    }());




    // ------------------------------------------------------------------
    // jQuery for back to Top
    // ------------------------------------------------------------------
    (function(){

          $('body').append('<div id="toTop"><i class="fa fa-angle-up"></i></div>');

            $(window).scroll(function () {
                if ($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });

        $('#toTop').on('click',function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });

    }());



/*    (function () {

        $('.testimonialSlider').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: false
        })

        // Navigation
        $('.prev').on('click', function(){
            $('.testimonialSlider').flexslider('prev')
            return false;
        })

        $('.next').on('click', function(){
            $('.testimonialSlider').flexslider('next')
            return false;
        })
    }());

*/


});
















$(document).on('click', '.m-menu .dropdown-menu', function(e) {
  e.stopPropagation()
})
